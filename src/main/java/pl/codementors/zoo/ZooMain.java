package pl.codementors.zoo;

import pl.codementors.zoo.model.*;

import java.io.*;
import java.util.Scanner;

/**
 * Main class for the application.
 *
 * @author Paweł Wieraszka
 */
public class ZooMain {

    /**
     * Application starting method.
     * @param args Command line application starting arguments.
     */
    public static void main(String[] args){

        /* Size of animal table. */
        int size;
        boolean running = true;
        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Hello to the ZOO\n");
        System.out.println("How many animals do you want to enter?");

        size = inputScanner.nextInt();

        /* Table with animals. */
        Animal[] zoo = new Animal[size];

        /* Entering animals (size times). */
        for (int i=0; i < size; i++){
            System.out.println("Enter type of your "+(i+1)+". animal: 1-parrot, 2-iguana, 3-wolf");
            int command = inputScanner.nextInt();
            System.out.println("What is the name of your animal?");
            String name = inputScanner.next();
            System.out.println("How old is your animal?");
            int age = inputScanner.nextInt();
            System.out.println("What color is your animal?");
            String color = inputScanner.next();

            /* Creating an animal depending on the type. */
            switch (command) {
                case 1: {
                    Animal animal = new Parrot(name,age,color);
                    zoo[i] = animal;
                    break;
                }
                case 2: {
                    Animal animal = new Iguana(name,age,color);
                    zoo[i] = animal;
                    break;
                }
                case 3: {
                    Animal animal = new Wolf(name,age,color);
                    zoo[i] = animal;
                    break;
                }
            }
        }

        /* Loop will read user input and make actions based on it. */
        while (running) {
            System.out.println("1 - print your animals\n" +
                    "2 - feed your animals\n" +
                    "3 - check your animals\n" +
                    "4 - feed meat eaters\n" +
                    "5 - feed plant eaters\n" +
                    "6 - print colors of your animals\n" +
                    "7 - save to the file\n" +
                    "8 - read from file\n" +
                    "9 - save binary to file\n" +
                    "10 - read binary from file\n" +
                    "0 - exit");

            System.out.print("Command: ");
            int command2 = inputScanner.nextInt();

            switch (command2) {
                case 1: {
                    print(zoo);
                    break;
                }
                case 2: {
                    feed(zoo);
                    break;
                }
                case 3: {
                    howl(zoo);
                    hiss(zoo);
                    screech(zoo);
                    break;
                }
                case 4: {
                    feedWithMeat(zoo);
                    break;
                }
                case 5: {
                    feedWithPlant(zoo);
                    break;
                }
                case 6: {
                    printColors(zoo);
                    break;
                }
                case 7: {
                    System.out.println("Enter the path with filename to save:");
                    inputScanner.nextLine();
                    saveToFile(zoo, inputScanner);
                    break;
                }
                case 8: {
                    System.out.println("Enter the path with filename to read:");
                    inputScanner.nextLine();
                    print(readFromFile(inputScanner));
                    break;
                }
                case 9: {
                    System.out.println("Enter the path with filename to save:");
                    inputScanner.nextLine();
                    saveToBinaryFile(zoo, inputScanner);
                    break;
                }
                case 10: {
                    System.out.println("Enter the path with filename to read:");
                    inputScanner.nextLine();
                    print(readFromBinaryFile(inputScanner));
                    break;
                }
                case 0: {
                    running = false;
                    break;
                }
            }
        }
    }

    /**
     * Prints all animals from the table.
     * @param animals Table with animals.
     */
    private static void print(Animal[] animals) {
        System.out.println();
        for (Animal a : animals) {
            if (a != null) {
                System.out.println("TYPE: "+ a.getClass().getSimpleName());
                System.out.println("NAME: " + a.getName());
                System.out.println("AGE: " + a.getAge());
                System.out.println();
            }
        }
    }

    /**
     * Feeds all animals from the table.
     * @param animals Table with animals
     */
    private static void feed(Animal[] animals) {
        System.out.println();
        for (Animal a : animals) {
            a.eat();
        }
    }

    /**
     * Saves information about animals from the table to the text file.
     * @param animals Table with animals.
     * @param input File name and path.
     */
    private static void saveToFile(Animal[] animals, Scanner input) {
        System.out.print("File: ");
        String fileName = input.nextLine();
        File file = new File(fileName);
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.write(animals.length+"\n");
            for (int i = 0; i < animals.length; i++) {
                if (animals[i] != null) {
                    bw.write(i+"\n");
                    if (animals[i] instanceof Parrot) {
                        bw.write("1\n");
                    }
                    if (animals[i] instanceof Iguana) {
                        bw.write("2\n");
                    }
                    if (animals[i] instanceof Wolf) {
                        bw.write("3\n");
                    }
                    bw.write(animals[i].getName()+"\n");
                    bw.write(animals[i].getAge()+"\n");
                }
            }
        }  catch (IOException ex) {
            System.err.println(ex);
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        }
    }

    /**
     * Reads information about animals from the text file and places them in the table.
     * @param input File name and path.
     * @return Table with animals.
     */
    private static Animal[] readFromFile(Scanner input) {
        System.out.print("File: ");
        String fileName = input.nextLine();
        File file = new File(fileName);
        Animal[] animals = new Animal[0];
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String sizeText = br.readLine();
            int size = Integer.parseInt(sizeText);
            animals = new Animal[size];
            while (br.ready()) {
                String indexText = br.readLine();
                String typeText = br.readLine();
                String name = br.readLine();
                String ageText = br.readLine();
                int index = Integer.parseInt(indexText);
                int type = Integer.parseInt(typeText);
                int age = Integer.parseInt(ageText);
                switch (type) {
                    case 1: {
                        Animal animal = new Parrot(name, age, "yellow");
                        animals[index] = animal;
                        break;
                    }
                    case 2: {
                        Animal animal = new Iguana(name, age, "green");
                        animals[index] = animal;
                        break;
                    }
                    case 3: {
                        Animal animal = new Wolf(name, age, "grey");
                        animals[index] = animal;
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
        return animals;
    }

    /**
     * Checks if an animal from the table is a wolf and if so calls howl method.
     * @param animals Table with animals.
     */
    private static void howl(Animal[] animals) {
        for (Animal a : animals) {
            if (a instanceof Wolf) {
                ((Wolf) a).howl();
            }
        }
    }

    /**
     * Checks if an animal from the table is an iguana and if so calls hiss method.
     * @param animals Table with animals.
     */
    private static void hiss(Animal[] animals) {
        for (Animal a : animals) {
            if (a instanceof Iguana) {
                ((Iguana) a).hiss();
            }
        }
    }

    /**
     * Checks if an animal from the table is a parrot and if so calls screech method.
     * @param animals Table with animals.
     */
    private static void screech(Animal[] animals) {
        for (Animal a : animals) {
            if (a instanceof Parrot) {
                ((Parrot) a).screech();
            }
        }
    }

    /**
     * Checks if an animal from the table is carnivorous and if so calls eatMeat method from Carnivorous interface.
     * @param animals Table with animals.
     */
    private static void feedWithMeat(Animal[] animals) {
        for (Animal a : animals) {
            if (a instanceof Carnivorous) {
                ((Carnivorous) a).eatMeat();
            }
        }
    }

    /**
     * Checks if an animal from the table is herbivorous and if so calls eatPlant method from Herbivorous interface.
     * @param animals Table with animals.
     */
    private static void feedWithPlant(Animal[] animals) {
        for (Animal a : animals) {
            if (a instanceof Herbivorous) {
                ((Herbivorous) a).eatPlant();
            }
        }
    }

    /**
     * Checks instance of an animal and prints its color of feather or scale or fur.
     * @param animals Table with animals.
     */
    private static void printColors(Animal[] animals) {
        for (Animal a : animals) {
            System.out.println("NAME: " + a.getName());
            if (a instanceof Bird) {
                System.out.println("COLOR: "+((Bird) a).getFeatherColor()+"\n");
            }
            else if (a instanceof Lizard) {
                System.out.println("COLOR: "+((Lizard) a).getScaleColor()+"\n");
            }
            else if (a instanceof Mammal) {
                System.out.println("COLOR: "+((Mammal) a).getFurColor()+"\n");
            }
        }
    }

    /**
     * Saves information about animals from the table to the binary file.
     * @param animals Table with animals.
     * @param input File name and path.
     */
    private static void saveToBinaryFile(Animal[] animals, Scanner input) {
        System.out.print("File: ");
        String fileName = input.nextLine();
        File file = new File(fileName);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(animals);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Reads information about animals from binary file and places them in the table.
     * @param input File name and path.
     * @return Table with animals.
     */
    private static Animal[] readFromBinaryFile(Scanner input) {
        System.out.print("File: ");
        String fileName = input.nextLine();
        File file = new File(fileName);
        Animal[] animals = new Animal[10];
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            animals = (Animal[]) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
        }
        return animals;
    }
}
