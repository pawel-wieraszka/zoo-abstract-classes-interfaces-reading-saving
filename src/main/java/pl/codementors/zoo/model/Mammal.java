package pl.codementors.zoo.model;

/**
 * Abstract class for mammal. Mammal class inherit from animal class.
 *
 * @author Paweł Wieraszka
 */
public abstract class Mammal extends Animal {

    /**
     * Fur color od mammal.
     */
    private String furColor;

    /**
     * Constructor with name and age of animal and fur color of mammal.
     * @param name Name of animal.
     * @param age Age of animal.
     * @param furColor Color of mammal fur.
     */
    public Mammal(String name, int age, String furColor) {
        super(name, age);
        this.furColor = furColor;
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
