package pl.codementors.zoo.model;

/**
 * Abstract class for lizard. Lizard class inherit from animal class.
 *
 * @author Paweł Wieraszka
 */
public abstract class Lizard extends Animal {

    /**
     * Scale color of lizard.
     */
    private String scaleColor;

    /**
     * Constructor with name and age of animal and scale color of lizard.
     * @param name Name of animal.
     * @param age Age of animal.
     * @param scaleColor Color of lizard scale.
     */
    public Lizard(String name, int age, String scaleColor) {
        super(name, age);
        this.scaleColor = scaleColor;
    }

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
