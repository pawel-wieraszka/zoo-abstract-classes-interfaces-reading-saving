package pl.codementors.zoo.model;

/**
 * Class for parrot. Parrot class inherit from Bird class and implements Herbivorous interface.
 */
public class Parrot extends Bird implements Herbivorous {

    /**
     * Constructor with name and age of animal and feather color of bird.
     * Parrot class extends Bird class, which extends Animal class.
     * @param name Name of animal.
     * @param age Age of animal.
     * @param featherColor Color of bird feather.
     */
    public Parrot(String name, int age, String featherColor) {
        super(name, age, featherColor);
    }

    /**
     * Screeching method.
     */
    public void screech() {
        System.out.println(getName()+": <screech>\n");
    }

    /**
     * Overwriting method from Bird class.
     */
    @Override
    public void eat() {
        System.out.println(getName()+" eats cwir, cwir\n");
    }

    /**
     * Overwriting method from Herbivorous interface.
     */
    @Override
    public void eatPlant() {
        System.out.println(getName()+":\nMniam, very tasty grain, mniam\n");
    }

}
