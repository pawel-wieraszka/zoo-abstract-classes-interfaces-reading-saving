package pl.codementors.zoo.model;

/**
 * Abstract class for bird.
 * Bird class inherit from Animal class.
 *
 * @author Paweł Wieraszka
 */
public abstract class Bird extends Animal {

    /**
     * Feather color of bird.
     */
    private String featherColor;

    /**
     * Constructor with name and age of animal and feather color of bird.
     * @param name Name of animal.
     * @param age Age of animal.
     * @param featherColor Color of bird feather.
     */
    public Bird(String name, int age, String featherColor) {
        super(name, age);
        this.featherColor = featherColor;
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
