package pl.codementors.zoo.model;

import java.io.Serializable;

/**
 * Abstract class for animal.
 *
 * @author Paweł Wieraszka
 */
public abstract class Animal implements Serializable {

    static final long serialVersionUID = 1;

    /**
     * Name of animal.
     */
    private String name;

    /**
     * Age of animal.
     */
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Abstract method is overwritten in classes, that inherit from the animal class.
     */
    public abstract void eat();
}
