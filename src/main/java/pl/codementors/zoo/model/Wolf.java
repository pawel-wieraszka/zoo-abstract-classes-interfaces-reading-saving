package pl.codementors.zoo.model;

/**
 * Class for wolf. Wolf class inherit from Mammal class and implements Carnivorous interface.
 */
public class Wolf extends Mammal implements Carnivorous {

    /**
     * Constructor with name and age of animal and feather color of bird.
     * Wolf class extends Mammal class, which extends Animal class.
     * @param name Name of animal.
     * @param age Age of animal.
     * @param furColor Color of wolf fur.
     */
    public Wolf(String name, int age, String furColor) {
        super(name, age, furColor);
    }

    /**
     * Howling method.
     */
    public void howl() {
        System.out.println(getName()+": <howl>\n");
    }

    /**
     * Overwriting method from Mammal class.
     */
    @Override
    public void eat() {
        System.out.println(getName()+" eats, hou houuuuuu\n");
    }

    /**
     * Overwriting method from Carnivorous interface.
     */
    @Override
    public void eatMeat() {
        System.out.println(getName()+":\nMniam, very tasty meat, mniam\n");
    }
}
