package pl.codementors.zoo.model;

/**
 * Class for iguana. Iguana class inherit from Lizard class and implements Herbivorous interface.
 *
 * @author Paweł Wieraszka
 */
public class Iguana extends Lizard implements Herbivorous {

    /**
     * Constructor with name and age of animal and scale color of lizard.
     * Iguana class extends Lizard class, which extends Animal class.
     * @param name Name of animal.
     * @param age Age of animal.
     * @param scaleColor Color of lizard scale.
     */
    public Iguana(String name, int age, String scaleColor) {
        super(name, age, scaleColor);
    }

    /**
     * Hissing method.
     */
    public void hiss() {
        System.out.println(getName()+": <hiss>\n");
    }

    /**
     * Overwriting method from Lizard class.
     */
    @Override
    public void eat() {
        System.out.println(getName()+" eatsssssssssss.\n");
    }

    /**
     * Overwriting method from Herbivorous interface.
     */
    @Override
    public void eatPlant() {
        System.out.println(getName()+":\nMniam, very tasty plants, mniam\n");
    }
}
