package pl.codementors.zoo.model;

/**
 * Interface Carnivorous with one abstract method.
 *
 * @author Paweł Wieraszka
 */
public interface Carnivorous {

    /**
     * Abstract method is overwritten in classes, that inherit from the animal class.
     */
    void eatMeat();
}
